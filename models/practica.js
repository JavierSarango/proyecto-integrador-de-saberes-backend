'use strict';
module.exports = (sequelize, DataTypes) => {
    const practica = sequelize.define('practica', {
        titulo: { type: DataTypes.STRING(255), defaultValue: "NO_DATA" },
        criterio_evaluacion: { type: DataTypes.STRING(255), defaultValue: "NO_DATA" },
        descripcion: { type: DataTypes.STRING(255), defaultValue: "NO_DATA" },
        fecha_limite: { type: DataTypes.DATE, defaultValue: sequelize.NOW },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }
    }, { freezeTableName: true });
    practica.associate = function (models) {
        practica.belongsTo(models.asignatura, { foreignKey: 'id_asignatura' });
        practica.belongsTo(models.persona, { foreignKey: 'id_persona' });
        practica.hasMany(models.entregable, { foreignKey: 'id_practica' });
    };
    return practica;
}