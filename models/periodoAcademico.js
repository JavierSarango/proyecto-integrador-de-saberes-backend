// Uso estricto del modo estricto para un código más seguro
'use strict';
module.exports = (sequalize, DataTypes) => {
    //Estructura de variables en Periodo Academico
    const periodoAcademico = sequalize.define('periodoAcademico', {
        nombre: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        descripcion: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        fecha_inicio: { type: DataTypes.DATE, defaultValue: sequelize.NOW },
        fecha_fin: { type: DataTypes.DATE, defaultValue: sequelize.NOW },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    //Relaciones de Periodo Academico
    periodoAcademico.associate = function (models) {
        periodoAcademico.hasOne(models.matricula, { foreignKey: 'id_periodoAcademico', as: 'matricula' });
    }

    // Retornar el modelo PeriodoAcademico
    return periodoAcademico;
}
