'use strict';
module.exports = (sequalize, DataTypes) => {
    //Estructura de variables en Curso
    const curso = sequalize.define('curso', {
        nombre: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        descripcion: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    //Relaciones de Curso
    curso.associate = function (models) {
        curso.hasOne(models.matricula, { foreignKey: 'id_curso', as: 'matricula' });
    }
    return curso;
}