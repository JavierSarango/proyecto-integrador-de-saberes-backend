'use strict';
module.exports = (sequalize, DataTypes) => {
    //Estructura de variables en asignatura
    const asignatura = sequalize.define('asignatura', {
        nombre: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    //Relaciones de Asignatura
    asignatura.associate = function (models) {
        asignatura.hasMany(models.practica, { foreignKey: 'id_practica', as: 'practica' });
    }
    return asignatura;
}