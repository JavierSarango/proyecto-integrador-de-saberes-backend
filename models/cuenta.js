'use strict';

module.exports = (sequelize, DataTypes) => {
    // Define el modelo 'cuenta' con sus propiedades y configuraciones
    const cuenta = sequelize.define('cuenta', {
        correo: { type: DataTypes.STRING(20), defaultValue: "NO_DATA", allowNull: false, unique: true }, // Correo electrónico único y no nulo
        clave: { type: DataTypes.STRING(60), defaultValue: "NO_DATA" }, // Propiedad para almacenar la clave (contraseña) de la cuenta
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }, // Propiedad booleana que indica el estado de la cuenta por defecto como activo
        estado: { type: DataTypes.BOOLEAN, defaultValue: true } // Propiedad booleana que indica el estado de la cuenta por defecto como activo
    }, { freezeTableName: true }); // Evita que Sequelize cambie el nombre de la tabla
 // Define las asociaciones del modelo 'cuenta' con otros modelos
    cuenta.associate = function (models) {
        // Establece una relación de pertenencia con el modelo 'persona'
        cuenta.belongsTo(models.persona, { foreignKey: 'id_persona' }); 
    };

    return cuenta; // Devuelve el modelo 'cuenta' configurado
};