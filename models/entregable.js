'use strict';
const sequelizeFile = require('sequelize-file');

module.exports = (sequelize, DataTypes) => {
    const entregable = sequelize.define('entregable', {
        archivo: {type: DataTypes.STRING(255), allowNull: true },
        fecha_entrega: { type: DataTypes.DATE, defaultValue: sequelize.NOW },
        contenido: { type: DataTypes.STRING(255), allowNull: true},
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }

    }, { freezeTableName: true });
    entregable.associate = function (models) {
        entregable.belongsTo(models.practica, { foreignKey: 'id_practica', as: 'practica' });
        
    };
    return entregable;
}