'use strict';

module.exports = (sequelize, DataTypes) => {
    // Define el modelo 'persona' con sus propiedades y configuraciones
    const persona = sequelize.define('persona', {
        nombres: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        apellidos: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        identificacion: { type: DataTypes.STRING(20), unique: true, allowNull: false, defaultValue: "NO_DATA" }, //unique campo unico
        tipo_identificacion: { type: DataTypes.ENUM("CEDULA", "PASAPORTE", "RUC"), allowNull: false, defaultValue: "CEDULA" }, // Enumeración y no nulo
        telefono: { type: DataTypes.STRING(20), defaultValue: "NO_DATA" },
        direccion: { type: DataTypes.STRING(255), allowNull: true, defaultValue: "NO_DATA" },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }, // Generación de UUID por defecto
        estado: { type: DataTypes.BOOLEAN, defaultValue: true } // Valor booleano por defecto
    }, { freezeTableName: true }); // Evita que Sequelize cambie el nombre de la tabla

    // Define las asociaciones del modelo 'persona' con otros modelos
    persona.associate = function (models) {
        persona.belongsTo(models.rol, { foreignKey: 'id_rol' });
        persona.hasOne(models.cuenta, { foreignKey: 'id_persona', as: 'cuenta' });
        
    };
    return persona; // Devuelve el modelo 'persona' configurado
};

    
