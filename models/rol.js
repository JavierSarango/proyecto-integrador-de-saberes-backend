'use strict';

// Define el modelo 'rol' con sus propiedades y configuraciones
module.exports = (sequelize, DataTypes) => {
    const rol = sequelize.define('rol', {
        nombre: { type: DataTypes.STRING(20) }, // Propiedad para almacenar el nombre del rol
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 },// Genera un UUID por defecto para identificación externa
        estado: { type: DataTypes.BOOLEAN, defaultValue: true }// Propiedad booleana que indica el estado del rol por defecto como activo
    }, { freezeTableName: true }); // Evita que Sequelize cambie el nombre de la tabla
// Define las asociaciones del modelo 'rol' con otros modelos
    rol.associate = function (models) {
        // Relación uno a muchos con el modelo 'persona'
        rol.hasMany(models.persona, { foreignKey: 'id_rol', as: 'persona' }); 
    };

    return rol; // Devuelve el modelo 'rol' configurado
};