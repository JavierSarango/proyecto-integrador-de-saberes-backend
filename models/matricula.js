'use strict';
module.exports = (sequalize, DataTypes) => {
    //Estructura de variables en Matricula
    const matricula = sequalize.define('matricula', {
        nombre: { type: DataTypes.STRING(100), defaultValue: "NO_DATA" },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });
    //Relaciones de Matricula
    matricula.associate = function (models) {
        matricula.belongsTo(models.persona, { foreignKey: 'id_persona' });
        matricula.belongsTo(models.curso, { foreignKey: 'id_curso' });
        matricula.belongsTo(models.periodoAcademico, { foreignKey: 'id_periodoAcademico' });
    }
    return matricula;
}