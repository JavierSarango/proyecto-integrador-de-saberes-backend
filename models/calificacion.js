'use strict';
module.exports = (sequelize, DataTypes) => {
    const calificacion = sequelize.define('calificacion', {
        observacion: {type: DataTypes.STRING(255), defaultValue: "NO_DATA"},
        nota: {type: DataTypes.FLOAT, allowNull: true, validate:{min: 0, max: 10}},
        external_id: {type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true}
    }, {freezeTableName: true});
    calificacion.associate = function(models){
           calificacion.belongsTo(models.practica,{foreignKey: 'id_practica'});
            
    };
    return calificacion;
}