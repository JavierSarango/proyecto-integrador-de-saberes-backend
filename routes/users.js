var express = require('express');
var router = express.Router();
const multer = require('multer');
const { body } = require('express-validator');
const path = require('path');
const RolController = require('../controls/RolController');
const PersonaController = require('../controls/PersonaController');
const CuentaController = require('../controls/CuentaController');
const TerminalController = require('../controls/TerminalController');
const CursoController = require("../controls/CursoController");
const AsignaturaController = require("../controls/AsignaturaController");
const PracticaController = require("../controls/PracticaController");
const EntregableController = require("../controls/EntregableController");
const MatriculaController = require("../controls/MatriculaController");
const PeriodoAcademicoController = require("../controls/PeriodoAcademicoController");

var rolController = new RolController();
var personaController = new PersonaController();
var cuentaController = new CuentaController();
var terminalController = new TerminalController();
var cursoController = new CursoController();
var asignaturaController = new AsignaturaController();
var practicaController = new PracticaController();
var entregableController = new EntregableController();
var matriculaController = new MatriculaController();
var periodoAcademicoController = new PeriodoAcademicoController();

let jwt = require('jsonwebtoken');

//middleware
var auth = function middleware(req, res, next) {
  const token = req.headers['x-api-token'];
  if (token) {
    require('dotenv').config();
    const llave = process.env.KEY;
    jwt.verify(token, llave, async (err, decoded) => {
      if (err) {
        console.log(err);
        res.status(401);
        res.json({ msg: "Token no valido o expirado", code: 401 });
      } else {
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({
          where: { external_id: req.decoded.external }
        });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Token no valido", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe token", code: 401 });
  }
}

/* GET users listing. */
router.get('/', function (req, res, next) {
  //res.send('respond with a resource');
  res.json({ "version": "1.0", "name": "auto" });
});

//CUENTA
router.post('/sesion', [
  body('email', 'Ingrese un correo valido!').trim().exists().not().isEmpty(),
  body('clave', 'Ingrese la clave').trim().exists().not().isEmpty()
], cuentaController.sesion);
//FIN CUENTA

router.get('/roles', rolController.listar);

//persona
router.post('/persona/guardar', [
  body('apellidos', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor o igual a 3 y menor a 100 "),
  body('nombres', 'Ingrese algun dato').trim().exists()
], personaController.guardar);
router.post('/persona/modificar', personaController.modificar);
router.get('/persona/listar', auth, personaController.listar);
router.get('/persona/obtener/:external', auth, personaController.obtener);

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});

router.get('/roles', rolController.listar);

//practica
router.post('/practica/guardar', practicaController.guardar);
router.get('/practica', practicaController.listar);
//entregables
router.post('/entregable/guardar/plano', entregableController.guardar);
router.get('/entregable', entregableController.listar);

//curso
router.post('/curso/guardar', cursoController.guardar);
router.get('/curso', cursoController.listar);

//asignatura
router.post('/asignatura/guardar', asignaturaController.guardar);
router.get('/asignatura', asignaturaController.listar);

//terminal
router.get('/terminal/:message', terminalController.leerDatos);

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/Entregables'); // Ruta donde se guardarán los entregables
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname)); // Nombre de archivo único
  }
});
const upload = multer({
  storage: storage,
  // limits: {
  //   fileSize: 2 * 1024 * 1024 
  // } 
});

// Ruta POST para guardar un entregable
router.post('/entregable/guardar', upload.single('archivo'), entregableController.guardar);

//Matricula
router.post('/matricula/guardar', matriculaController.guardar);
router.get('/matricula', matriculaController.listar);

//Periodo Academico
router.post('/periodo/guardar', periodoAcademicoController.guardar);
router.get('/periodo', periodoAcademicoController.listar);

module.exports = router;

