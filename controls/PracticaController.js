'use strict';

const { body, validationResult, check } = require('express-validator');
var models = require('../models');

var persona = models.persona;
var practica = models.practica;
var asignatura = models.asignatura;
const bcrypt = require('bcrypt');
const saltRounds = 8; //Puede ser cualquier número


class PracticaController {

    async listar(req, res) {
        //Averiguar para el míercoles cómo sacar el correo y mostrar solo el correo
        //console.log("Entro en listar")
        var lista = await practica.findAll({
            include: [
                { model: models.asignatura, as: 'asignatura', attributes: ['nombre'] },
                { model: models.persona, as: 'persona', attributes: ['nombres', 'apellidos', 'identificacion'] }
            ],
            attributes: ['titulo', 'criterio_evaluacion', 'fecha_limite', 'descripcion', 'external_id', 'estado']
        });
        //console.log("Esta es lista",lista)
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await practica.findOne({
            where: { external_id: external }, include: { model: models.cuenta },
            attributes: ['titulo', 'criterio_evaluacion', 'descripcion', 'fecha_limite', 'estado', 'external_id'] //lista los atributos que requiera 
        }); //
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })

    }

    async guardar(req, res) {
        console.log("Entro a guardarrrrrrrrrr")
       
        let errors = validationResult(req);

        if (errors.isEmpty()) {
            console.log(req.body.identificacion+" y estooo22  " + req.body.external_asignatura)
            let asignatura_id = req.body.external_asignatura; //req.body es para los post
            let persona_id = req.body.identificacion;
            console.log(persona_id+" y estooo  " + asignatura_id)
            if (asignatura_id != undefined && persona_id != undefined) {
                let asignaturaAux = await asignatura.findOne({ where: { external_id: asignatura_id } });
               
                let personaAux = await persona.findOne({ where: { identificacion: persona_id } });
                var fechaLimite = new Date(req.body.fecha_limite + 'T00:00:00'); 
                console.log(fechaLimite)
                
                var fechaActual = new Date();
                console.log("dffffffffff "+fechaActual)
                if (fechaLimite < fechaActual) {
                    res.status(400);
                    return res.json({ msg: "La fecha límite debe ser a partir de la fecha actual", code: 400 });
                }else if(fechaLimite >= fechaActual){
                    var data = {
                        titulo: req.body.titulo, //en la parte derecha es como está en la base de datos, a la derecha lo puedo llamar como quiera (Debe ser documentado)
                        criterio_evaluacion: req.body.criterio_evaluacion,
                        fecha_limite: fechaLimite,
                        descripcion: req.body.descripcion,
                        id_asignatura: asignaturaAux.id,
                        id_persona: personaAux.id
                       
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await practica.create(data,transaction);
                        await transaction.commit();
                        res.json({ msg: "Se han registrado sus datos", code: 200 });

                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {

                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                    //console.log(transaction);

                }


            } else {
                res.status(400);
                console.log("Entro a Faltan datos")
                res.json({ msg: "Faltan datos", code: 400 });
            }



        } else {
            res.status(400);
            console.log("Entro a datos Faltantes")
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }

    }
    async modificar(req, res) {

        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var person = await persona.findOne({ where: { external_id: req.body.external } });
            if (person === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
                //TODO
            } else {
                var uuid = require('uuid');
                person.identificacion = req.body.dni;//en la parte derecha es como está en la base de datos, a la derecha lo puedo llamar como quiera (Debe ser documentado)
                person.tipo_identificacion = req.body.tipo;
                person.nombres = req.body.nombres;
                person.apellidos = req.body.apellidos;
                person.direccion = req.body.direccion;
                person.external_id = uuid.v4();
                var result = await person.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }

    }


}
module.exports = PracticaController;