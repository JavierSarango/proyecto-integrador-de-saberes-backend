'use strict'; // Habilita el modo estricto para un código más seguro y restrictivo

var models = require('../models'); // Importa los modelos definidos en otros archivos
var rol = models.rol; // Importa el modelo 'rol'

class RolController{
    async listar(req, res){
        // Obtiene una lista de registros 'rol' con atributos específicos
        var lista = await rol.findAll({
            attributes:['nombre','external_id','estado'] // Atributos específicos a mostrar
        }); //
        res.json({msg: "OK!", code:200, info: lista})
    }


}
module.exports = RolController; // Exporta la clase 'RolController'