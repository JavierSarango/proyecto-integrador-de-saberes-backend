'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var periodoAcademico = models.periodoAcademico;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class periodoAcademicoController {
    //Metodo de listado de Periodo Academico
    async listar(req, res) {
        var lista = await periodoAcademico.findAll({
            attributes: ['nombre', 'descripcion', 'fecha_inicio', 'fecha_fin', 'estado']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
    //Metodo de obtencion por el external_id de Periodo Academico
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await periodoAcademico.findOne({
            attributes: ['nombre', 'descripcion', 'fecha_inicio', 'fecha_fin', 'estado']
        });
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
    //Metodo de guardado de Periodo Academico
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var fechaFin = new Date(req.body.fecha_fin + 'T00:00:00');
            var FechaInicio = new Date(req.body.fecha_inicio + 'T00:00:00');
            if (fechaFin < FechaInicio) {
                res.status(400);
                return res.json({ msg: "La fecha fin debe ser a partir de la fecha incio", code: 400 });
            } else if (fechaFin >= FechaInicio) {
                var data = {
                    nombre: req.body.nombre,
                    descripcion: req.body.descripcion,
                    fecha_fin: fechaFin,
                    fecha_inicio: FechaInicio,
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();
                try {
                    await periodoAcademico.create(data, transaction);
                    await transaction.commit();
                    res.json({ msg: "Se han registrado sus datos", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.errors && error.errors[0].message) {
                        res.json({ msg: error.errors[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
            }
        } else {
            res.status(400);
            console.log("Entro a datos Faltantes")
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    //Metodo de modificación por external_id de Periodo Academico
    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var period = await periodoAcademico.findOne({ where: { external_id: req.body.external } });
            if (period === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
            } else {
                var uuid = require('uuid');
                period.nombre = req.body.nombre;
                period.descripcion = req.body.descripcion;
                period.fecha_inicio = req.body.fecha_inicio;
                period.fecha_fin = req.body.fecha_fin;
                period.external_id = uuid.v4();
                var result = await period.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

}
module.exports = periodoAcademicoController;