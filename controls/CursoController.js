'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var curso = models.curso;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class CursoController {
    //Metodo de listado de curso
    async listar(req, res) {
        var lista = await curso.findAll({
            attributes: ['nombre', 'descripcion', 'estado']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
    //Metodo de obtencion por el external_id de curso
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await curso.findOne({
            where: { external_id: external }, include: { model: models.curso },
            attributes: ['nombre', 'descripcion', 'estado']
        });
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
    //Metodo de guardado de curso
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var data = {
                nombre: req.body.nombre,
                descripcion: req.body.descripcion
            };
            res.status(200);
            let transaction = await models.sequelize.transaction();
            try {
                await curso.create(data);
                res.json({ msg: "Se han registrado sus datos", code: 200 });
            } catch (error) {
                if (transaction) await transaction.rollback();
                if (error.errors && error.errors[0].message) {
                    res.json({ msg: error.errors[0].message, code: 200 });
                } else {
                    res.json({ msg: error.message, code: 200 });
                }
            }
            console.log(transaction);
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    //Metodo de modificación por external_id de curso
    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var curs = await curso.findOne({ where: { external_id: req.body.external } });
            if (curs === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
            } else {
                var uuid = require('uuid');
                curs.descripcion = req.body.descripcion;
                curs.nombre = req.body.nombre;
                curs.external_id = uuid.v4();
                var result = await curs.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

}
module.exports = CursoController;