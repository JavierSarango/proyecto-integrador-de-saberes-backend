'use strict'; // Habilita el modo estricto para un código más seguro y restrictivo

const { validationResult } = require("express-validator"); // Importa la función de validación de Express
var models = require('../models/');// Importa los modelos definidos en otros archivos
var cuenta = models.cuenta; // Importa el modelo 'cuenta'
const bcrypt = require('bcrypt'); // Importa la librería de hashing bcrypt
const saltRounds = 8; // Define el número de rondas de sal para bcrypt
let jwt = require('jsonwebtoken'); // Importa la librería JWT para autenticación

class CuentaController {
    async sesion (req, res){
        // Realiza validación de datos y maneja la sesión de inicio de sesión
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var login = await cuenta.findOne({
                where:{correo:req.body.email}, // Busca la cuenta por correo electrónico
                include: {model: models.persona, attributes: ['apellidos', 'nombres','identificacion'],
            include: [{model: models.rol, attributes:['nombre']}]} // Incluye el modelo 'persona' y 'rol' en la consulta
            });
            if (login===null) {
                res.status(400);
                res.json({msg: "Cuenta no encontrada",code: 400, errors:errors});
            } else {
                
                res.status(200);

                var isClaveValida = function(clave, claveUser){
                    // Compara la contraseña ingresada con la almacenada en la cuenta
                    return bcrypt.compareSync(claveUser,clave);
                };
                
                if (login.estado) {
                    //valido
                    if(isClaveValida(login.clave, req.body.clave)){
                        // Genera un token JWT si la contraseña es válida
                        const tokenData = {
                            external: login.external_id,
                            email: login.correo,
                            check: true
                        };
                        require('dotenv').config(); // Carga las variables de entorno desde .env
                        //Se obtiene la llave del archivo .env
                        const llave = process.env.KEY; 
                        //jwt.sign firma
                        // Firma un token JWT con los datos del usuario y la clave secreta 
                        const token = jwt.sign(tokenData, llave, {expiresIn:'12h'});
                         // Devuelve una respuesta con información del usuario y el token
                        res.json({
                            msg: "OK!",
                            token : token,
                            user : login.persona.nombres+' '+login.persona.apellidos,
                            identificacion: login.persona.identificacion,
                            correo : login.correo,
                            rol: login.persona.rol.nombre,
                            code: 200

                        });
                    }else{
                        res.json({msg: "Clave incorrecta!",code: 400, errors:errors});
                    }
                } else {
                    
                    res.json({msg: "Cuenta desactivada",code: 400, errors:errors});
                    
                }
                
                
            }
        } else {
            res.status(400);
            res.json({msg: "Datos faltantes",code: 400, errors:errors});
        }
    }

}
module.exports = CuentaController; // Exporta la clase 'CuentaController'