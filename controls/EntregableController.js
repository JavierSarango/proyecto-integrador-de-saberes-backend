'use strict';

const { body, validationResult, check } = require('express-validator');
var models = require('../models');

var persona = models.persona;
var practica = models.practica;
const bcrypt = require('bcrypt');
const entregable = models.entregable;
const saltRounds = 8; //Puede ser cualquier número
const express = require('express');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const router = express.Router();


class EntregableController {

    async listar(req, res) {
        //Averiguar para el míercoles cómo sacar el correo y mostrar solo el correo
        //console.log("Entro en listar")
        var lista = await entregable.findAll({
            include: [
                { model: models.practica, as: 'practica', attributes: ['titulo'] }],
            attributes: ['contenido', 'fecha_entrega','archivo', 'external_id', 'estado'] //lista los atributos que requiera 
        }); //
        //console.log("Esta es lista",lista)
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await practica.findOne({
            where: { external_id: external }, include: { model: models.cuenta },
            attributes: ['nombres', 'apellidos', 'direccion', 'identificacion', 'tipo_identificacion', 'external_id'] //lista los atributos que requiera 
        }); //
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })

    }

    // async guardar(req, res) {
    //     let errors = validationResult(req);

    //     if (errors.isEmpty()) {
    //         let practica_id = req.body.external_practica; //req.body es para los post
    //         // let persona_id = req.body.identificacion;
    //         if (practica_id != undefined) {
    //             let practicaAux = await practica.findOne({ where: { external_id: practica_id } });
    //             console.log(practicaAux);
    //                 var data = {
    //                     archivo: req.body.archivo, //en la parte derecha es como está en la base de datos, a la derecha lo puedo llamar como quiera (Debe ser documentado)
    //                     fecha_entrega: req.body.fecha_entrega,
    //                     contenido: req.body.contenido,

    //                 };
    //                 res.status(200);
    //                 let transaction = await models.sequelize.transaction();
    //                 try {
    //                     await entregable.create(data,transaction);
    //                     await transaction.commit();
    //                     res.json({ msg: "Se han registrado sus datos", code: 200 });

    //                 } catch (error) {
    //                     if (transaction) await transaction.rollback();
    //                     if (error.errors && error.errors[0].message) {
    //                         res.json({ msg: error.errors[0].message, code: 200 });
    //                     } else {

    //                         res.json({ msg: error.message, code: 200 });
    //                     }
    //                 }
    //                 console.log(transaction);

    //             // } else {
    //             //     res.status(400);
    //             //     res.json({ msg: "Datos no encontrados", code: 400 });
    //             // }


    //         } else {
    //             res.status(400);
    //             res.json({ msg: "Faltan datos", code: 400 });
    //         }

    //     } else {
    //         res.status(400);
    //         res.json({ msg: "Datos faltantes", code: 400, errors: errors });
    //     }

    // }

    async guardar(req, res) {
        let errors = validationResult(req);

        if (errors.isEmpty()) {
            //console.log("---------------"+req.file.size)
            if (req.file && req.file.size > 2 * 1024 * 1024) {
                res.status(400);
                return res.json({ msg: "El archivo es demasiado grande (máximo 2MB)", code: 400 });
            } else {
                var practica_id = req.body.id_practica;
                if (practica_id !== undefined) {
                    console.log(practica_id)
                    let practicaAux = await practica.findOne({ where: { external_id: practica_id } });
                    console.log(practicaAux.id)
                    if (practicaAux) {
                        // Obtener el nombre del archivo desde la solicitud

                        var data = {
                            archivo: req.file.filename,
                            contenido: req.body.contenido,
                            fecha_entrega: req.body.fecha_entrega,
                            id_practica: practicaAux.id
                        };

                        res.status(200);
                        let transaction = await models.sequelize.transaction();
                        try {
                            await entregable.create(data, { transaction });
                            await transaction.commit();
                            res.json({ msg: "Se han registrado sus datos", code: 200 });

                        } catch (error) {
                            if (transaction) await transaction.rollback();
                            if (error.errors && error.errors[0].message) {
                                res.json({ msg: error.errors[0].message, code: 200 });
                            } else {

                                res.json({ msg: error.message, code: 200 });
                            }
                        }
                    } else {
                        res.status(400);
                        res.json({ msg: "Práctica no encontrada", code: 400 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Faltan datos", code: 400 });
                }

            }



        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes o inválidos", code: 400, errors: errors.array() });
        }
    }


    async modificar(req, res) {

        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var entregables = await entregable.findOne({ where: { external_id: req.body.external } });
            if (entregables === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
                //TODO
            } else {
                var uuid = require('uuid');
                entregables.archivo = req.body.archivo;//en la parte derecha es como está en la base de datos, a la derecha lo puedo llamar como quiera (Debe ser documentado)
                entregables.fecha_entrega = req.body.fecha_entrega;
                entregables.contenido = req.body.contenido;
                entregables.external_id = uuid.v4();
                var result = await entregables.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }

    }


}
module.exports = EntregableController;