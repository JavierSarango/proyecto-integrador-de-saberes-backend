'use strict';// Habilita el modo estricto para un código más seguro y restrictivo

const { body, validationResult, check } = require('express-validator'); // Importa funciones de validación de Express
var models = require('../models'); // Importa los modelos definidos en Sequelize
var rol = models.rol; // Modelo 'rol'
var persona = models.persona; // Modelo 'persona'
var cuenta = models.cuenta; // Modelo 'cuenta'
const bcrypt = require('bcrypt'); // Importa la librería de encriptación bcrypt
const saltRounds = 8; // Número de rondas para la generación de salt en bcrypt


class PersonaController {
 // Método para listar personas con información de cuenta
    async listar(req, res) {
        var lista = await docente.findAll({
            include: { model: models.cuenta, as: 'cuenta', attributes: ['correo'] },
            attributes: ['nombres', 'apellidos', 'direccion', 'identificacion', 'tipo_identificacion', 'external_id'] //lista los atributos que requiera 
        }); //
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }

// Método para obtener información de una persona por external_id
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external }, include: { model: models.cuenta },
            attributes: ['nombres', 'apellidos', 'direccion', 'identificacion', 'tipo_identificacion', 'external_id'] //lista los atributos que requiera 
        }); //
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })

    }

 // Método para guardar nueva persona con cuenta asociada
    async guardar(req, res) {
        // Realiza validación de datos y procesa el guardado de una nueva 'persona' con su 'cuenta' asociada
        let errors = validationResult(req); // Valida si hay errores en la solicitud

        if (errors.isEmpty()) {
            let rol_id = req.body.external_rol; 
            if (rol_id != undefined) {
                let rolAux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolAux);

                if (rolAux) {
                    // Función para hashear la contraseña
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);
                    };
                     // Datos para crear una nueva 'persona' y su 'cuenta' asociada
                    var data = {
                        identificacion: req.body.dni, 
                        tipo_identificacion: req.body.tipo,
                        nombres: req.body.nombres,
                        apellidos: req.body.apellidos,
                        direccion: req.body.direccion,
                        telefono: req.body.telefono,
                        id_rol: rolAux.id,

                        cuenta: {
                            correo: req.body.correo,
                            clave: claveHash(req.body.clave)

                        }
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        // Creación de 'persona' y su 'cuenta' en una transacción
                        await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                        await transaction.commit();
                        res.json({ msg: "Se han registrado sus datos", code: 200 });

                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0].message) {
                            res.json({ msg: error.errors[0].message, code: 200 });
                        } else {

                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                    console.log(transaction);

                } else {
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }


            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }



        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }

    }// Método para modificar los datos de una persona
    async modificar(req, res) {
        // Realiza validación de datos y procesa la modificación de un registro 'persona'
        let errors = validationResult(req); // Valida si hay errores en la solicitud
        if (errors.isEmpty()) {
            var persona = await persona.findOne({ where: { external_id: req.body.external } });
            if (persona === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
                //TODO
            } else {
                var uuid = require('uuid');
                persona.identificacion = req.body.dni;
                persona.tipo_identificacion = req.body.tipo;
                persona.nombres = req.body.nombres;
                persona.apellidos = req.body.apellidos;
                persona.direccion = req.body.direccion;
                persona.external_id = uuid.v4();
                var result = await persona.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }

    }


}
module.exports = PersonaController; // Exporta el controlador de personas
