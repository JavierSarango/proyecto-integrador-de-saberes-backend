'use strict';
const axios = require("axios");
const { response } = require("express");

class TerminalController {
    //Controlador de peticiones al servidor externo
    async leerDatos(req, res) {
        try {
            //Envio de comando por URL al servido externo
            const response = await axios.get('http://10.20.203.222/?message=' + req.params.comando);
            if (response.status == 200) {
                //Obtención de datos
                const listar = response;
                res.json({
                    msg: 'OK!',
                    code: 200,
                    info: listar.data
                });
            } else {
                res.json({
                    msg: "Error al intentar conectar con el servidor",
                    code: response.status,
                    info: []
                });
            }
        } catch (error) {
            res.json({
                msg: "Error al realizar la solicitud al servidor",
                code: response.status,
                error: error,
                info: []
            });
        }
    }
}
module.exports = TerminalController;
