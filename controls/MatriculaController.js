'use strict';
const { body, validationResult, check } = require('express-validator');
var models = require('../models');
var matricula = models.matricula;
var persona = models.persona;
var curso = models.curso;
var periodoAcademico = models.periodoAcademico;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class MatriculaController {
    //Metodo de listado de matricula
    async listar(req, res) {
        var lista = await matricula.findAll({
            include: [
                { model: models.curso, as: 'curso', attributes: ['nombre', 'descripcion', 'estado'] },
                { model: models.persona, as: 'persona', attributes: ['nombres', 'apellidos', 'identificacion', 'tipo_identificacion'] },
                { model: models.periodoAcademico, as: 'periodoAcademico', attributes: ['nombre', 'descripcion', 'fecha_inicio', 'fecha_fin'] }
            ],
            attributes: ['nombre', 'estado']
        });
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
    //Metodo de obtencion por el external_id de matricula
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await matricula.findOne({
            where: { external_id: external }, include: { model: models.cuenta },
            where: { external_id: external }, include: { model: models.persona },
            where: { external_id: external }, include: { model: models.periodoAcademico },
            attributes: ['nombre', 'estado']
        });
        if (lista === null) {
            lista = {}
        }
        res.status(200);
        res.json({ msg: "OK!", code: 200, info: lista })
    }
    //Metodo de guardado de matricula
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            let curso_id = req.body.external_curso;
            let periodoAcademico_id = req.body.external_periodoAcademico;
            let persona_id = req.body.external_persona;
            if (curso_id != undefined && periodoAcademico_id != undefined && persona_id != undefined) {
                let cursoAux = await curso.findOne({ where: { external_id: curso_id } });
                let periodoAcademicoAux = await periodoAcademico.findOne({ where: { external_id: periodoAcademico_id } });
                let personaAux = await persona.findOne({ where: { external_id: persona_id } });
                var data = {
                    nombre: req.body.nombre,
                    id_curso: cursoAux.id,
                    id_periodoAcademico: periodoAcademicoAux.id,
                    id_persona: personaAux.id
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();
                try {
                    await matricula.create(data, transaction);
                    await transaction.commit();
                    res.json({ msg: "Se han registrado sus datos", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.errors && error.errors[0].message) {
                        res.json({ msg: error.errors[0].message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
            } else {
                res.status(400);
                console.log("Entro a Faltan datos")
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            console.log("Entro a datos Faltantes")
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    //Metodo de modificación por external_id de matricula
    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var matric = await matricula.findOne({ where: { external_id: req.body.external } });
            if (matric === null) {
                res.status(400);
                res.json({ msg: "No existen registros", code: 400 });
            } else {
                var uuid = require('uuid');
                matric.nombre = req.body.nombre;
                matric.estado = req.body.estado;
                matric.external_id = uuid.v4();
                var result = await matric.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se han modificado sus datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Se han modificado sus datos", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

}
module.exports = MatriculaController;